import React, {Component} from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom';

import Home from './Components/Home';
import Add from './Containers/Add'
import Quote from './Components/Quote'
import './App.css';
import Edit from "./Containers/Edit";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={Home}></Route>
                    <Route path="/quote/:id" component={Home}></Route>
                    <Route path="/add" component={Add}></Route>
                    <Route path="/edit/:name/:id" component={Edit}></Route>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;
