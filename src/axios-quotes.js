import axios from 'axios'

const instance = axios.create({
    baseURL:'https://cw-8-niakoks.firebaseio.com/'
});

instance.interceptors.request.use(req =>{
    console.log('[In request interceptor]',req);
    return req;
});
instance.interceptors.response.use(res =>{
    console.log('[In response success interceptor]',res);
    return res;
    },error => {
    console.log('[In response err interceptor]',error);
    throw error;
});

export default instance;