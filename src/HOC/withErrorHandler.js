import React, {Component, Fragment} from 'react'

import Spinner from "../Components/UI/Spinner/Spinner";

const withErrorHendler = (WrappedComponent, axios) => {
    return class WithErrorHOC extends Component {
        constructor(props) {
            super(props);
            this.state = {
                error:null,
                loading: false,
            };

            this.state.interceptorIdreq = axios.interceptors.request.use(req =>{
                this.setState({loading: true});
                return req;
            });

            this.state.interceptorIdres = axios.interceptors.response.use(res => {
                this.setState({loading:false});
                return res
            },error =>{
                this.setState({error});
                throw error
            });
        }

         render(){
            return(
                <Fragment>
                    <WrappedComponent{...this.props}/>
                    {this.state.loading?<Spinner/>:null}
                </Fragment>
            )

        }

    }
};
export default withErrorHendler;
