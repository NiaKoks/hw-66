import React, {Component,Fragment} from 'react';
import axios from '../axios-quotes';
import Quote from '../Components/Quote.js'
class Add extends Component {
    addQuote = (quote) =>{
        console.log(quote);
        axios.post(`/quotes/${quote.category}.json`,quote).then((response)=>{
            this.props.history.replace('/')
        })
    }
    render() {
        return (
            <Fragment>
                <h2>Add new Quote</h2>
                <Quote onSubmit={this.addQuote}/>
            </Fragment>
        );
    }
}

export default Add;