import React, {Component} from 'react';
import {CATEGORIES} from "../Categories";
import Nav from "./UI/Nav";
import "../Containers/Form.css";
import axios from  'axios';
import withErrorHandler from "../HOC/withErrorHandler";


class Quote extends Component {
    state = {
        category: Object.keys(CATEGORIES)[0],
        text :'',
        author:'',
    };

    valueChanged = event =>{
        const {name,value} = event.target;
        this.setState({[name]: value});
    };

    submitHandler=event=>{
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <div className="new-quote">
                <Nav/>
                <form className="quote-form" onSubmit={(event) => this.submitHandler(event)}>

                    <p>Category</p>
                    <select name="category" onChange={this.valueChanged} value={this.state.category}>
                        {Object.keys(CATEGORIES).map(catId =>(
                            <option key={catId} value={catId}>{CATEGORIES[catId]}</option>
                        ))}
                    </select>

                    <p>Author</p>
                    <input type="text" value={this.state.author}
                           onChange={this.valueChanged}
                           className="quote-author"
                           name="author"/>

                    <p>Quote text:</p>
                    <textarea value={this.state.text}
                              onChange={this.valueChanged}
                              className="quote-text" cols="30" rows="10"
                              name="text"/>

                    <button type='submit' className="add-quote-btn">Submit</button>
                </form>
            </div>
        );
    }
}

export default withErrorHandler(Quote,axios);